﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace JJCommerce.Models
{
    public class User
    {
        [Key]
        public int UserID { get; set; }


        [Required(ErrorMessage = "The field {0} is required")]
        [MaxLength(256, ErrorMessage = "The field {0} must be maximun {1} characters length")]
        [Index("User_UserName_Index", IsUnique = true)]
        [Display(Name = "Mail")]
        [DataType(DataType.EmailAddress)]
        public string UserName { get; set; }

        [Required(ErrorMessage = "The field {0} is required")]
        [MaxLength(50, ErrorMessage = "The field {0} must be maximun {1} characters length")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "The field {0} is required")]
        [MaxLength(50, ErrorMessage = "The field {0} must be maximun {1} characters length")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [MaxLength(20, ErrorMessage = "El campo {0} debe tener como máximo {1} caracteres")]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Teléfono")]
        public string Phone { get; set; }

        [DataType(DataType.ImageUrl)]
        [Display(Name = "Foto de perfil")]
        public string Photo { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Range(1, double.MaxValue, ErrorMessage = "Necesitas seleccionar un {0}")]
        [Display(Name = "Provincia")]
        public int ProvinceID { get; set; }
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Range(1, double.MaxValue, ErrorMessage = "Necesitas seleccionar un {0}")]
        [Display(Name = "Localidad")]
        public int LocationID { get; set; }
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Range(1, double.MaxValue, ErrorMessage = "Necesitas seleccionar un {0}")]
        [Display(Name = "Compañía")]
        public int CompanyID { get; set; }

        //propiedad de lectura
        [Display(Name = "User")]
        public string FullName { get { return string.Format("{0} {1}", FirstName, LastName); } }

        [NotMapped]
        public HttpPostedFileBase PhotoFile { get; set; }

        public virtual Province Province { get; set; }
        public virtual Location Location { get; set; }
        public virtual Company Company { get; set; }

    }
}