﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace JJCommerce.Models
{
    public class Warehouse
    {
        [Key]
        public int WarehouseID { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Range(1, double.MaxValue, ErrorMessage = "Necesitas seleccionar un {0}")]
        [Index("Warehouse_CompanyID_WarehouseName_Index", 1, IsUnique = true)]
        [Display(Name = "Compañía")]
        public int CompanyID { get; set; }

        [Required(ErrorMessage = "The field {0} is required")]
        [MaxLength(50, ErrorMessage = "The field {0} must be maximun {1} characters length")]
        [Index("Warehouse_CompanyID_WarehouseName_Index", 2, IsUnique = true)]
        [Display(Name = "Depósito")]
        public string WarehouseName { get; set; }



        [Required(ErrorMessage = "El campo {0} es requerido")]
        [MaxLength(20, ErrorMessage = "El campo {0} debe tener como máximo {1} caracteres")]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Teléfono")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "The field {0} is required")]
        [MaxLength(100, ErrorMessage = "The field {0} must be maximun {1} characters length")]
       
        [Display(Name = "Dirección")]
        public string Adress { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Range(1, double.MaxValue, ErrorMessage = "Necesitas seleccionar un {0}")]
        [Display(Name = "Provincia")]
        public int ProvinceID { get; set; }
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Range(1, double.MaxValue, ErrorMessage = "Necesitas seleccionar un {0}")]
        [Display(Name = "Localidad")]
        public int LocationID { get; set; }

        public virtual Province Province { get; set; }
        public virtual Location Location { get; set; }
        public virtual Company Company { get; set; }
        public virtual ICollection<Inventory> Inventories { get; set; }
    }
}