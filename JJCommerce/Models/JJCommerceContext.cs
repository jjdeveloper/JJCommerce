﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace JJCommerce.Models
{
    public class JJCommerceContext : DbContext
    {
        public JJCommerceContext() : base("DefaultConnection")
        {

        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }


        public DbSet<Province> Provinces { get; set; }

        public System.Data.Entity.DbSet<JJCommerce.Models.Location> Locations { get; set; }

        public System.Data.Entity.DbSet<JJCommerce.Models.Company> Companies { get; set; }

        public System.Data.Entity.DbSet<JJCommerce.Models.User> Users { get; set; }

        public System.Data.Entity.DbSet<JJCommerce.Models.Category> Categories { get; set; }

        public System.Data.Entity.DbSet<JJCommerce.Models.Tax> Taxes { get; set; }

        public System.Data.Entity.DbSet<JJCommerce.Models.Product> Products { get; set; }

        public System.Data.Entity.DbSet<JJCommerce.Models.Warehouse> Warehouses { get; set; }

        public System.Data.Entity.DbSet<JJCommerce.Models.Inventory> Inventories { get; set; }

        public System.Data.Entity.DbSet<JJCommerce.Models.Customer> Customers { get; set; }

        public System.Data.Entity.DbSet<JJCommerce.Models.Status> Status { get; set; }

        public System.Data.Entity.DbSet<JJCommerce.Models.Order> Orders { get; set; }

        public System.Data.Entity.DbSet<JJCommerce.Models.OrderDetail> OrderDetails { get; set; }

        public System.Data.Entity.DbSet<JJCommerce.Models.OrderDetailTmp> OrderDetailTmps { get; set; }

        public System.Data.Entity.DbSet<JJCommerce.Models.CompanyCustomer> CompanyCustomers { get; set; }
    }
}