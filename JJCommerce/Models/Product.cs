﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace JJCommerce.Models
{
    public class Product
    {
        [Key]
        public int ProductID { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Range(1, double.MaxValue, ErrorMessage = "Necesitas seleccionar un {0}")]
        [Display(Name = "Compañía")]
        [Index("Product_CompanyID_Description_Index", 1, IsUnique = true)]
        [Index("Product_CompanyID_BarCode_Index", 1, IsUnique = true)]
        public int CompanyID { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [MaxLength(50, ErrorMessage = "El campo {0} debe tener como máximo {1} caracteres")]
        [Display(Name = "Producto")]
        [Index("Product_CompanyID_Description_Index", 2, IsUnique = true)]
        public string Description { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [MaxLength(13, ErrorMessage = "El campo {0} debe tener como máximo {1} caracteres")]
        [Display(Name = "Código de barras")]
        [Index("Product_CompanyID_BarCode_Index", 2, IsUnique = true)]
        public string BarCode { get; set; }


        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Range(1, double.MaxValue, ErrorMessage = "Necesitas seleccionar un {0}")]
        [Display(Name = "Categoría")]
        public int CategoryID { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Range(1, double.MaxValue, ErrorMessage = "Necesitas seleccionar un {0}")]
        [Display(Name = "Impuesto")]
        public int TaxID { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [DisplayFormat(DataFormatString = "{0:C2}", ApplyFormatInEditMode = false)]
        [Range(0, double.MaxValue, ErrorMessage = "You must sleect a {0} between {1} and {2}")]
        [Display(Name = "Precio")]
        public decimal Price { get; set; }
        [Display(Name = "Imagen")]
        [DataType(DataType.ImageUrl)]
        public string Image { get; set; }

       
        [Display(Name = "Comentarios")]
        [DataType(DataType.MultilineText)]
        public string Remarks { get; set; }

        [NotMapped]
        [Display(Name = "Imagen")]
        public HttpPostedFileBase ImageFile { get; set; }

        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = false)]
        public double Stock { get { return Inventories == null ? 0 : Inventories.Sum(i => i.Stock); } }
        public virtual Company Company { get; set; }
        public virtual Category Category { get; set; }
        public virtual Tax Tax { get; set; }
        public virtual ICollection<Inventory> Inventories { get; set; }
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
        public virtual ICollection<OrderDetailTmp> OrderDetailTmps { get; set; }
    }
}