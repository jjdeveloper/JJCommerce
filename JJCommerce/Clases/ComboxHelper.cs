﻿using JJCommerce.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;

namespace JJCommerce.Clases
{
    public class ComboxHelper : IDisposable
    {
        private static JJCommerceContext db = new JJCommerceContext();
        public static List<Province> GetProvinces()
        {
            var provinces = db.Provinces.ToList();
            provinces.Add(new Province
            {
                ProvinceID = 0,
                ProvinceName = "[Seleccione una provincia...]"
            });
            return provinces.OrderBy(d => d.ProvinceName).ToList();
        }

        public static List<Product> GetProducts(int companyId, bool sw)
        {
            var products = db.Products.Where(p => p.CompanyID == companyId).ToList();
            return products.OrderBy(p => p.Description).ToList();
        }


        public static List<Product> GetProducts(int companyID)
        {
            var products = db.Products.Where(c => c.CompanyID == companyID).ToList();
            products.Add(new Product
            {
                ProductID = 0,
                Description = "[Seleccione un producto...]"
            });

            return products.OrderBy(c => c.Description).ToList();
        }

        public static List<Location> GetLocations(int provinceId)
        {
            var locations = db.Locations.Where(p => p.ProvinceID == provinceId).ToList();
            locations.Add(new Location
            {
                LocationID = 0,
                LocationName = "[Seleccione una localidad...]"
            });
            return locations.OrderBy(d => d.LocationName).ToList();
        }

        public static List<Company> GetCompanies()
        {
            var companies = db.Companies.ToList();
            companies.Add(new Company
            {
                CompanyID = 0,
                CompanyName = "[Seleccione una compañía...]"
            });
            return companies.OrderBy(d => d.CompanyName).ToList();
        }

        public void Dispose()
        {
            db.Dispose();
        }

        public static List<Customer> GetCustomers(int companyID)
        {
            var qry = (from cu in db.Customers
                       join cc in db.CompanyCustomers on cu.CustomerID equals cc.CustomerID
                       join co in db.Companies on cc.CompanyID equals co.CompanyID
                       where co.CompanyID == companyID
                       select new { cu }).ToList();

            var customers = new List<Customer>();
            foreach (var item in qry)
            {
                customers.Add(item.cu);
            }

            customers.Add(new Customer
            {
                CustomerID = 0,
                FirstName = "[Seleccione un cliente...]"
            });
            return customers.OrderBy(c => c.FirstName).ThenBy(c => c.LastName).ToList();
        }

        public static List<Category> GetCategories(int companyID)
        {
            var categories = db.Categories.Where(c => c.CompanyID == companyID).ToList();
            categories.Add(new Category
            {
                CategoryID = 0,
                Description = "[Seleccione una categoría...]"
            });
            return categories.OrderBy(d => d.Description).ToList();
        }

        public static List<Tax> GetTaxes(int companyID)
        {
            var taxes = db.Taxes.Where(c => c.CompanyID == companyID).ToList();
            taxes.Add(new Tax
            {
                TaxID = 0,
                Description = "[Seleccione un impuesto...]"
            });
            return taxes.OrderBy(d => d.Description).ToList();
        }
    }
}