﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JJCommerce.Models;
using JJCommerce.Clases;

namespace JJCommerce.Controllers
{
    [Authorize(Roles = "User")]
    public class CustomersController : Controller
    {
        private JJCommerceContext db = new JJCommerceContext();

        // GET: Customers
        public ActionResult Index()
        {
            var user = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();

            var qry = (from cu in db.Customers
                       join cc in db.CompanyCustomers on cu.CustomerID equals cc.CustomerID
                       join co in db.Companies on cc.CompanyID equals co.CompanyID
                       where co.CompanyID == user.CompanyID
                       select new { cu }).ToList();

            var customers = new List<Customer>();
            foreach (var item in qry)
            {
                customers.Add(item.cu);
            }

            return View(customers.ToList());
        }

        // GET: Customers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var customer = db.Customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        // GET: Customers/Create
        public ActionResult Create()
        {

           // ViewBag.CompanyID = new SelectList(db.Companies, "CompanyID", "CompanyName");
            ViewBag.LocationID = new SelectList(ComboxHelper.GetLocations(0), "LocationID", "LocationName");
            ViewBag.ProvinceID = new SelectList(ComboxHelper.GetProvinces(), "ProvinceID", "ProvinceName");
         
            return View();
        }

        // POST: Customers/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Customer customer)
        {
            if (ModelState.IsValid)
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.Customers.Add(customer);
                        var response = DBHelper.SaveChanges(db);
                        if (!response.Succeeded)
                        {
                            ModelState.AddModelError(string.Empty, response.Message);
                            transaction.Rollback();
                            ViewBag.LocationID = new SelectList(db.Locations, "LocationID", "LocationName", customer.LocationID);
                            ViewBag.ProvinceID = new SelectList(db.Provinces, "ProvinceID", "ProvinceName", customer.ProvinceID);
                            return View(customer);
                        }

                        UsersHelper.CreateUserASP(customer.UserName, "Customer");

                        var user = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
                        var CompanyCustomer = new CompanyCustomer
                        {
                            CompanyID = user.CompanyID,
                            CustomerID = customer.CustomerID,
                        };
                        db.CompanyCustomers.Add(CompanyCustomer);
                        db.SaveChanges();

                        transaction.Commit();

                        return RedirectToAction("Index");
                    }

                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        ModelState.AddModelError(string.Empty, ex.Message);
                    }
                }
               
            }

            //ViewBag.CompanyID = new SelectList(db.Companies, "CompanyID", "CompanyName", customer.CompanyID);
            ViewBag.LocationID = new SelectList(db.Locations, "LocationID", "LocationName", customer.LocationID);
            ViewBag.ProvinceID = new SelectList(db.Provinces, "ProvinceID", "ProvinceName", customer.ProvinceID);
            return View(customer);
        }

        // GET: Customers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var customer = db.Customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            ViewBag.LocationID = new SelectList(ComboxHelper.GetLocations(customer.ProvinceID), "LocationID", "LocationName");
            ViewBag.ProvinceID = new SelectList(ComboxHelper.GetProvinces(), "ProvinceID", "ProvinceName");
            return View(customer);
        }

        // POST: Customers/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Customer customer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(customer).State = EntityState.Modified;
                var response = DBHelper.SaveChanges(db);
                if (response.Succeeded)
                {
                    //TODO: Validate when the customer email change
                    return RedirectToAction("Index");
                }

                ModelState.AddModelError(string.Empty, response.Message);
              
            }
            ViewBag.LocationID = new SelectList(ComboxHelper.GetLocations(customer.ProvinceID), "LocationID", "LocationName");
            ViewBag.ProvinceID = new SelectList(ComboxHelper.GetProvinces(), "ProvinceID", "ProvinceName");
            return View(customer);
        }

        // GET: Customers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var customer = db.Customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        // POST: Customers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var customer = db.Customers.Find(id);
            var user = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            var companyCustomer = db.CompanyCustomers.Where(cc => cc.CompanyID == user.CompanyID && cc.CustomerID == customer.CustomerID).FirstOrDefault();

            using (var transaction = db.Database.BeginTransaction())
            {
                db.CompanyCustomers.Remove(companyCustomer);
                db.Customers.Remove(customer);
                var response = DBHelper.SaveChanges(db);

                if (response.Succeeded)
                {
                    transaction.Commit();
                    return RedirectToAction("Index");
                }

                transaction.Rollback();
                ModelState.AddModelError(string.Empty, response.Message);
                return View(customer); 
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
