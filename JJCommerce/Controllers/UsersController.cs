﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JJCommerce.Models;
using JJCommerce.Clases;

namespace JJCommerce.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UsersController : Controller
    {
        private JJCommerceContext db = new JJCommerceContext();

        // GET: Users
        public ActionResult Index()
        {
            var users = db.Users.Include(u => u.Company).Include(u => u.Location).Include(u => u.Province);
            return View(users.ToList());
        }

        // GET: Users/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            ViewBag.CompanyID = new SelectList(ComboxHelper.GetCompanies(), "CompanyID", "CompanyName");
            ViewBag.LocationID = new SelectList(ComboxHelper.GetLocations(0), "LocationID", "LocationName");
            ViewBag.ProvinceID = new SelectList(ComboxHelper.GetProvinces(), "ProvinceID", "ProvinceName");
            return View();
        }

        // POST: Users/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(User user)
        {
            if (ModelState.IsValid)
            {
                db.Users.Add(user);
                db.SaveChanges();
                UsersHelper.CreateUserASP(user.UserName, "User");

                if (user.PhotoFile != null)
                {

                    var folder = "~/Content/Users";
                    var file = string.Format("{0}.jpg", user.UserID);
                    var response = FileHelper.UploadPhoto(user.PhotoFile, folder, file);
                    if (response)
                    {
                        var pic = string.Format("{0}/{1}", folder, file);
                        user.Photo = pic;
                        db.Entry(user).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }
                return RedirectToAction("Index");
            }

            ViewBag.CompanyID = new SelectList(ComboxHelper.GetCompanies(), "CompanyID", "CompanyName", user.CompanyID);
            ViewBag.LocationID = new SelectList(ComboxHelper.GetLocations(user.ProvinceID), "LocationID", "LocationName", user.LocationID);
            ViewBag.ProvinceID = new SelectList(ComboxHelper.GetProvinces(), "ProvinceID", "ProvinceName", user.ProvinceID);
            return View(user);
        }

        // GET: Users/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            ViewBag.CompanyID = new SelectList(ComboxHelper.GetCompanies(), "CompanyID", "CompanyName", user.CompanyID);
            ViewBag.LocationID = new SelectList(ComboxHelper.GetLocations(user.ProvinceID), "LocationID", "LocationName", user.LocationID);
            ViewBag.ProvinceID = new SelectList(ComboxHelper.GetProvinces(), "ProvinceID", "ProvinceName", user.ProvinceID);
            return View(user);
        }

        // POST: Users/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(User user)
        {
            if (ModelState.IsValid)
            {
                if (user.PhotoFile != null)
                {
              
                    var folder = "~/Content/Users";
                    var file = string.Format("{0}.jpg", user.UserID);
                    var response = FileHelper.UploadPhoto(user.PhotoFile, folder, file);
                    user.Photo = string.Format("{0}/{1}", folder, file);
                }

                var db2 = new JJCommerceContext();
                var currentUser = db2.Users.Find(user.UserID);
                if(currentUser.UserName != user.UserName)
                {
                    UsersHelper.UpdateUser(currentUser.UserName, user.UserName);

                }
                db2.Dispose();

                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CompanyID = new SelectList(ComboxHelper.GetCompanies(), "CompanyID", "CompanyName", user.CompanyID);
            ViewBag.LocationID = new SelectList(ComboxHelper.GetLocations(user.ProvinceID), "LocationID", "LocationName", user.LocationID);
            ViewBag.ProvinceID = new SelectList(ComboxHelper.GetProvinces(), "ProvinceID", "ProvinceName", user.ProvinceID);
            return View(user);
        }

        // GET: Users/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            User user = db.Users.Find(id);
            db.Users.Remove(user);
            db.SaveChanges();
            UsersHelper.DeleteUser(user.UserName, "User");
            return RedirectToAction("Index");
        }

     

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
