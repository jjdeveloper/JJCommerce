﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JJCommerce.Models;
using JJCommerce.Clases;

namespace JJCommerce.Controllers
{
    [Authorize(Roles = "User")]
    public class ProductsController : Controller
    {
        private JJCommerceContext db = new JJCommerceContext();

        // GET: Products
        public ActionResult Index()
        {
            var user = db.Users
                .Where(u => u.UserName == User.Identity.Name)
                .FirstOrDefault();
            var products = db.Products
                .Include(p => p.Category)
                .Include(p => p.Tax)
                .Where(p => p.CompanyID == user.CompanyID);
            return View(products.ToList());
        }

        // GET: Products/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // GET: Products/Create
        public ActionResult Create()
        {
            var user = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            ViewBag.CategoryID = new SelectList(ComboxHelper.GetCategories(user.CompanyID), "CategoryID", "Description");
            // ViewBag.CompanyID = new SelectList(db.Companies, "CompanyID", "CompanyName");
            ViewBag.TaxID = new SelectList(ComboxHelper.GetTaxes(user.CompanyID), "TaxID", "Description");
            var product = new Product { CompanyID = user.CompanyID, };

            return View(product);
        }

        // POST: Products/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Product product)
        {
            var user = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            if (ModelState.IsValid)
            {
                db.Products.Add(product);
                db.SaveChanges();

                if(product.ImageFile != null)
                {
                    var folder = "~/Content/Products";
                    var file = string.Format("{0}.jpg", product.ProductID);
                    var response = FileHelper.UploadPhoto(product.ImageFile, folder, file);
                    if (response)
                    {
                        product.Image = string.Format("{0}/{1}", folder, file);
                        
                        db.Entry(product).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }
                return RedirectToAction("Index");
            }

            ViewBag.CategoryID = new SelectList(ComboxHelper.GetCategories(user.CompanyID), "CategoryID", "Description", product.CategoryID);
           // ViewBag.CompanyID = new SelectList(db.Companies, "CompanyID", "CompanyName", product.CompanyID);
            ViewBag.TaxID = new SelectList(ComboxHelper.GetTaxes(user.CompanyID), "TaxID", "Description", product.TaxID);
            return View(product);
        }

        // GET: Products/Edit/5
        public ActionResult Edit(int? id)
        {
            var user = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }

            ViewBag.CategoryID = new SelectList(ComboxHelper.GetCategories(user.CompanyID), "CategoryID", "Description", product.CategoryID);
            //ViewBag.CompanyID = new SelectList(db.Companies, "CompanyID", "CompanyName", product.CompanyID);
            ViewBag.TaxID = new SelectList(ComboxHelper.GetTaxes(user.CompanyID), "TaxID", "Description", product.TaxID);
            return View(product);
        }

        // POST: Products/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Product product)
        {
            var user = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            if (ModelState.IsValid)
            {
                if (product.ImageFile != null)
                {
                    
                    var folder = "~/Content/Products";
                    var file = string.Format("{0}.jpg", product.ProductID);
                    var response = FileHelper.UploadPhoto(product.ImageFile, folder, file);
                    if (response)
                    {
                        product.Image = string.Format("{0}/{1}", folder, file);
               

                    }
                }

                db.Entry(product).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CategoryID = new SelectList(ComboxHelper.GetCategories(user.CompanyID), "CategoryID", "Description", product.CategoryID);
            //ViewBag.CompanyID = new SelectList(db.Companies, "CompanyID", "CompanyName", product.CompanyID);
            ViewBag.TaxID = new SelectList(ComboxHelper.GetTaxes(user.CompanyID), "TaxID", "Description", product.TaxID);
            return View(product);
        }

        // GET: Products/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var product = db.Products.Find(id);
            db.Products.Remove(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
