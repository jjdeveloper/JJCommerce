﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JJCommerce.Models;
using JJCommerce.Clases;

namespace JJCommerce.Controllers
{
    [Authorize(Roles = "User")]
    public class WarehousesController : Controller
    {
        private JJCommerceContext db = new JJCommerceContext();

        // GET: Warehouses
        public ActionResult Index()
        {
            var user = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            var warehouses = db.Warehouses.Where(w => w.CompanyID == user.CompanyID).Include(w => w.Location).Include(w => w.Province);
            return View(warehouses.ToList());
        }

        // GET: Warehouses/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Warehouse warehouse = db.Warehouses.Find(id);
            if (warehouse == null)
            {
                return HttpNotFound();
            }
            return View(warehouse);
        }

        // GET: Warehouses/Create
        public ActionResult Create()
        {
           // ViewBag.CompanyID = new SelectList(db.Companies, "CompanyID", "CompanyName");
            ViewBag.LocationID = new SelectList(ComboxHelper.GetLocations(0), "LocationID", "LocationName");
            ViewBag.ProvinceID = new SelectList(ComboxHelper.GetProvinces(), "ProvinceID", "ProvinceName");
            var user = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            var warehouse = new Warehouse
            {
                CompanyID = user.CompanyID,
            };
            return View(warehouse);
        }

        // POST: Warehouses/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Warehouse warehouse)
        {
            if (ModelState.IsValid)
            {
                db.Warehouses.Add(warehouse);
                var response = DBHelper.SaveChanges(db);
                if(response.Succeeded)
                {
                    return RedirectToAction("Index");

                }
                ModelState.AddModelError(string.Empty, response.Message);
                
            }

           // ViewBag.CompanyID = new SelectList(db.Companies, "CompanyID", "CompanyName", warehouse.CompanyID);
            ViewBag.LocationID = new SelectList(ComboxHelper.GetLocations(warehouse.ProvinceID), "LocationID", "LocationName", warehouse.LocationID);
            ViewBag.ProvinceID = new SelectList(ComboxHelper.GetProvinces(), "ProvinceID", "ProvinceName", warehouse.ProvinceID);
            return View(warehouse);
        }

        // GET: Warehouses/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var warehouse = db.Warehouses.Find(id);

            if (warehouse == null)
            {
                return HttpNotFound();
            }
            //ViewBag.CompanyID = new SelectList(db.Companies, "CompanyID", "CompanyName", warehouse.CompanyID);
            ViewBag.LocationID = new SelectList(ComboxHelper.GetLocations(warehouse.ProvinceID), "LocationID", "LocationName", warehouse.LocationID);
            ViewBag.ProvinceID = new SelectList(ComboxHelper.GetProvinces(), "ProvinceID", "ProvinceName", warehouse.ProvinceID);
            return View(warehouse);
        }

        // POST: Warehouses/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Warehouse warehouse)
        {
            if (ModelState.IsValid)
            {
                db.Entry(warehouse).State = EntityState.Modified;
                var response = DBHelper.SaveChanges(db);
                if(response.Succeeded)
                {
                    return RedirectToAction("Index");
                }

                ModelState.AddModelError(string.Empty, response.Message);
               
            }
           // ViewBag.CompanyID = new SelectList(db.Companies, "CompanyID", "CompanyName", warehouse.CompanyID);
            ViewBag.LocationID = new SelectList(ComboxHelper.GetLocations(warehouse.ProvinceID), "LocationID", "LocationName", warehouse.LocationID);
            ViewBag.ProvinceID = new SelectList(ComboxHelper.GetProvinces(), "ProvinceID", "ProvinceName", warehouse.ProvinceID);
            return View(warehouse);
        }

        // GET: Warehouses/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var warehouse = db.Warehouses.Find(id);
            if (warehouse == null)
            {
                return HttpNotFound();
            }
            return View(warehouse);
        }

        // POST: Warehouses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var warehouse = db.Warehouses.Find(id);
            db.Warehouses.Remove(warehouse);
            var response = DBHelper.SaveChanges(db);
            if(response.Succeeded)
            {
                return RedirectToAction("Index");
            }

            ModelState.AddModelError(string.Empty, response.Message);
            return View(warehouse);
        }

  

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
