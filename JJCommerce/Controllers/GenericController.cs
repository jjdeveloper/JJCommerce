﻿using JJCommerce.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JJCommerce.Controllers
{
    public class GenericController : Controller
    {
        private JJCommerceContext db = new JJCommerceContext();
        //GET JSON
        public JsonResult GetLocations(int provinceid)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var locations = db.Locations.Where(l => l.ProvinceID == provinceid);
            return Json(locations);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}