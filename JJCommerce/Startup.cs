﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(JJCommerce.Startup))]
namespace JJCommerce
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
