﻿$(document).ready(function () {
    $("#ProvinceID").change(function () {
        $("#LocationID").empty();
        $("#LocationID").append('<option value="0">[Seleccione una localidad...]</option>');
        $.ajax({
            type: 'POST',
            url: Url,
            dataType: 'json',
            data: { provinceid: $("#ProvinceID").val() },
            success: function (data) {
                $.each(data, function (i, data) {
                    $("#LocationID").append('<option value="'
                     + data.LocationID + '">'
                     + data.LocationName + '</option>');
                });
            },
            error: function (ex) {
                alert('Failed to retrieve locations.' + ex);
            }
        });
        return false;
    })
});